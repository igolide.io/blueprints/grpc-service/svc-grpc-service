package io.igolide.blueprints.tests.services

import io.igolide.blueprints.client.HealthClient
import io.igolide.blueprints.client.TokenCredentials
import io.igolide.blueprints.core.SpringTests
import io.igolide.blueprints.grpc.api.Api.HealthBundle.StatusRequest
import io.igolide.blueprints.utils.TokenIssuer
import org.springframework.beans.factory.annotation.Autowired
import org.testng.Assert
import org.testng.annotations.Test

class HealthServiceTests : SpringTests() {
    @Autowired
    lateinit var healthClient: HealthClient

    @Test
    fun statusTest() {
        val responce = healthClient
            .blockingStub()
            .withCallCredentials(TokenCredentials(TokenIssuer.issue("Voksiv")))
            .status(StatusRequest.getDefaultInstance())
        Assert.assertEquals(responce.status, "OK")
    }
}
