package io.igolide.blueprints.configurations

import io.igolide.blueprints.core.ApplicationContext
import io.igolide.blueprints.core.security.JwtAuthentication
import io.igolide.blueprints.core.security.JwtAuthenticationProvider
import net.devh.boot.grpc.server.security.authentication.BasicGrpcAuthenticationReader
import net.devh.boot.grpc.server.security.authentication.BearerAuthenticationReader
import net.devh.boot.grpc.server.security.authentication.CompositeGrpcAuthenticationReader
import net.devh.boot.grpc.server.security.authentication.GrpcAuthenticationReader
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.ProviderManager

@Configuration
class GrpcSecurityConfiguration {
    @Bean
    fun grpcAuthenticationReader(): GrpcAuthenticationReader {
        return BasicGrpcAuthenticationReader()
    }

    @Bean
    fun authenticationManager(): AuthenticationManager {
        val providers: MutableList<AuthenticationProvider> = mutableListOf()
        val provider = ApplicationContext.getBean(JwtAuthenticationProvider::class.java)
        providers.add(provider)
        return ProviderManager(providers)
    }

    @Bean
    fun authenticationReader(): GrpcAuthenticationReader {
        val readers: MutableList<GrpcAuthenticationReader> = ArrayList()
        readers.add(BearerAuthenticationReader { accessToken: String? -> JwtAuthentication(accessToken) })
        return CompositeGrpcAuthenticationReader(readers)
    }
}
