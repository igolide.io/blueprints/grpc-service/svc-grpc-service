package io.igolide.blueprints.services.health

import io.grpc.stub.StreamObserver
import io.igolide.blueprints.core.ApplicationContext
import io.igolide.blueprints.grpc.api.Api.HealthBundle.StatusRequest
import io.igolide.blueprints.grpc.api.Api.HealthBundle.StatusResponse
import io.igolide.blueprints.grpc.api.HealthServiceGrpc
import io.igolide.blueprints.services.health.operations.StatusOperation
import net.devh.boot.grpc.server.service.GrpcService
import org.springframework.security.access.annotation.Secured

@GrpcService
class HealthService : HealthServiceGrpc.HealthServiceImplBase() {
    @Secured("ROLE_USER")
    override fun status(request: StatusRequest, responseObserver: StreamObserver<StatusResponse>) {
        ApplicationContext.getBean(StatusOperation::class.java).execute(request, responseObserver)
    }
}
