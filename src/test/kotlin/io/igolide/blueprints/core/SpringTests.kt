package io.igolide.blueprints.core

import io.igolide.blueprints.utils.TokenIssuer
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPublicKey
import java.util.Base64

@SpringBootTest
abstract class SpringTests : AbstractTestNGSpringContextTests() {
    init {
        val keyPair = generateRSAKeyPair()
        val rsaPublicKey = keyPair.public as RSAPublicKey
        val encoder = Base64.getEncoder()
        val publicKey = encoder.encodeToString(rsaPublicKey.encoded)
        System.setProperty("RSA_PUBLIC_KEY", publicKey)
        TokenIssuer.init(keyPair)
    }

    private final fun generateRSAKeyPair(): KeyPair {
        val generator = KeyPairGenerator.getInstance("RSA")
        generator.initialize(512)
        return generator.generateKeyPair()
    }
}
