package io.igolide.blueprints.client

import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import org.springframework.stereotype.Component

@Component
class ChannelFactory {
    private val host = "localhost"
    private val port = 9090
    final var channel: ManagedChannel = ManagedChannelBuilder
        .forAddress(host, port)
        .usePlaintext()
        .build()
}
