package io.igolide.blueprints.client

import io.igolide.blueprints.grpc.api.HealthServiceGrpc
import io.igolide.blueprints.grpc.api.HealthServiceGrpc.HealthServiceBlockingStub
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class HealthClient {
    @Autowired
    private lateinit var channelFactory: ChannelFactory

    fun blockingStub(jwt: String? = null): HealthServiceBlockingStub {
        return HealthServiceGrpc.newBlockingStub(channelFactory.channel)
    }
}
