package io.igolide.blueprints.core

import io.grpc.stub.StreamObserver

abstract class Operation<in Request, Response> {

    fun execute(request: Request, observer: StreamObserver<Response>) {
        try {
            val response = executeInternal(request)
            observer.onNext(response)
        } catch (exp: Exception) {
            observer.onError(exp)
        }
        observer.onCompleted()
    }

    protected abstract fun executeInternal(request: Request): Response
}
