### Local Requirements

* Install [Java 17](https://java.tutorials24x7.com/blog/how-to-install-openjdk-17-on-mac)
* Install [Docker](https://docs.docker.com/get-docker/)
* Install [docker-compose](https://docs.docker.com/compose/install/)

#### Work Guarantied With:

```
$ java --version
openjdk 17.0.2 2022-01-18
OpenJDK Runtime Environment (build 17.0.2+8-86)
OpenJDK 64-Bit Server VM (build 17.0.2+8-86, mixed mode, sharing)
```

```
$ docker -v
Docker version 20.10.16
```

```
$ docker-compose -v
docker-compose version 1.27.4
```

### Verification

```
$ ./gradlew check
```

### Local service run

```
$ export RSA_PUBLIC_KEY=<YOUR_RSA_PUBLIC_KEY_HERE>
$ ./gradlew bootRun
```

### Local dockerized service run

If You want to run dockerized service, then go to local deployments and run docker-compose deployment

```
$ cd deployments/local
$ docker-compose pull
$ docker-compose up -d
```

### Documentation

* [API](https://gitlab.com/igolide.io/blueprints/grpc-service/api-grpc-service/-/blob/main/specification/api.proto)
* [JWT](https://jwt.io/)
* [GRPC](https://grpc.io/docs/)
* [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features)
* [Spring Security](https://docs.spring.io/spring-security/reference/index.html)
* [Spring Security Architecture](https://spring.io/guides/topicals/spring-security-architecture)
* [GRPC Spring boot starter](https://yidongnan.github.io/grpc-spring-boot-starter/en/)
* [Gradle](https://docs.gradle.org/current/userguide/userguide.html)
* [Kotlin](https://kotlinlang.org/docs/reference/)
* [kotlinter](https://github.com/jeremymailen/kotlinter-gradle)
* [Testng](https://testng.org/doc/documentation-main.html)
* [Docker](https://docs.docker.com/)
* [docker-compose](https://docs.docker.com/compose/)
* [Gitlab](https://docs.gitlab.com/ee/README.html)
* [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
* [Sonarqube](https://docs.sonarqube.org/latest/)
