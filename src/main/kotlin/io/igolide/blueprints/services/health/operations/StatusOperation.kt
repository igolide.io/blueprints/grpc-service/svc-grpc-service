package io.igolide.blueprints.services.health.operations

import io.igolide.blueprints.core.Operation
import io.igolide.blueprints.grpc.api.Api.HealthBundle.StatusRequest
import io.igolide.blueprints.grpc.api.Api.HealthBundle.StatusResponse
import org.springframework.stereotype.Service

@Service
class StatusOperation : Operation<StatusRequest, StatusResponse>() {
    override fun executeInternal(request: StatusRequest): StatusResponse {
        val response = StatusResponse.newBuilder()
        response.status = "OK"
        return response.build()
    }
}
