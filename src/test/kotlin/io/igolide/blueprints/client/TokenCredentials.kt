package io.igolide.blueprints.client

import io.grpc.CallCredentials
import io.grpc.Metadata
import io.grpc.Status
import java.util.concurrent.Executor

class TokenCredentials(private val token: String) : CallCredentials() {
    override fun applyRequestMetadata(requestInfo: RequestInfo, appExecutor: Executor, applier: MetadataApplier) {
        appExecutor.execute {
            try {
                val headers = Metadata()
                val authorizationKey = Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER)
                headers.put(authorizationKey, "Bearer $token")
                applier.apply(headers)
            } catch (ex: Throwable) {
                applier.fail(Status.UNAUTHENTICATED.withCause(ex))
            }
        }
    }

    override fun thisUsesUnstableApi() {
        // Should be a noop but never called; tries to make it clearer to implementors that they may break in the future.
    }
}
